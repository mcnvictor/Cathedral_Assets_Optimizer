<?xml version="1.0" ?><!DOCTYPE TS><TS language="ko_KR" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>작업을 실제로 진행하지 않고 변경되는 파일의 목록만 보여줍니다.</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>폴더 선택</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>실행</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>단일 모드</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>테스트 모드</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>고급 모드</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>스카이림SE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;모드 폴더에 존재하는 모든 BSA의 압축을 해제 합니다. &lt;span style=&quot; font-weight:600;&quot;&gt;주의!&lt;/span&gt; 이 옵션을 활성화 할 경우 작업 속도가 매우 느려질 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>BSA 압축 해제</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;작업 후 원본 BSA 파일의 백업을 생성하지 않습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>백업 파일 삭제</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;기존 파일을 묶어 신규 BSA 파일을 생성합니다. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;주의!&lt;/span&gt; 이 옵션을 활성화 할 경우 처리속도가 매우 느려질 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>BSA 생성</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>고급 설정</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>모로윈드</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>오블리비언</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>스카이림 / 폴아웃 3 / 폴아웃 뉴 베가스</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>폴아웃 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>최대 용량</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>확장명</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>파일 이름</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>메쉬</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;필수적인 최적화만 진행합니다. 헤드 파츠를 포함해 CtD를 유발할 수 있는 메쉬의 복구를 시도합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>필수 최적화</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>추가 최적화</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;모든 메쉬를 완전히 최적화합니다. 밸런스 모드에서 충분히 최적화되지 않았을 경우에 적용하십시오. 메쉬 품질이 더 낮아질 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>전체 최적화</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>항상 헤드 파츠 변환</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>메쉬 재저장</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>오블리비언 (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>폴아웃 3 / 스카이림 / 스카이림 SE / 폴아웃 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>User</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>오블리비언 (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>폴아웃 3 / 스카이림 / 스카이림 SE / 폴아웃 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Stream</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>오블리비언 / 폴아웃 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>스카이림 (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>스카이림 SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>폴아웃 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>텍스처</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>리사이즈</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>높이:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>폭:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>비율 유지</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>고정 크기</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>TGA 변환 활성화</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>프로필</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>신규 프로필</translation>
    </message>
    <message>
        <source>New</source>
        <translation>새 프로필</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;작업을 실제로 진행하지 않고 변경되는 파일의 리스트만 로그에 출력합니다. BSA에 들어있는 파일은 무시됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>모드 묶음</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>BSA 작업</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;파일의 최대용량을 GB 단위로 지정합니다. 본 값을 늘리면 BSA에 들어가는 파일의 갯수는 늘어나지만, BSA 용량 한계값에 도달할 경우 CtD가 발생할 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BSA 파일의 확장명을 변경합니다. 일반적으로 스카이림의 경우 &amp;quot;.bsa&amp;quot; 이며, 폴아웃 4는 &amp;quot;.ba2&amp;quot; 를 사용합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;폴아웃 4의 경우 - Main.ba2 .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>텍스처를 개별 BSA로 압축</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;스카이림SE의 경우 - Textures.bsa .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>전문가 설정</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSA 형식</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>텍스처 BSA 형식</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>메쉬 변환</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;필수적인 최적화와 함께 일부 메쉬를 최적화합니다. 해당 메쉬로 인한 인게임 오류를 줄여주지만, 메쉬 품질이 다소 낮아질 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;본 옵션을 해제할 경우 헤드 파츠를 변환하지 않습니다. 여러 모드를 동시에 처리할 때는 해제하는 것을 권장합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;최적화 옵션과 무관하게 NifSkope에서 제공하는 기본 최적화 스크립트를 적용하고 재저장 합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>텍스처 변환</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;스카이림 SE에서 읽어오지 못하는 모든 TGA 파일을 DDS로 변환합니다. 또한 SSE와 호환되지 않는 구버전 확장명 등, CtD를 유발할 수 있는 텍스처도 모두 변환합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;설정된 포맷으로 무손실 텍스처를 압축합니다. 스카이림 SE와 폴아웃 4 사용자에게 권장합니다. 다른 게임에서는 품질이 저하될 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>텍스처 압축</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;텍스처 밉맵을 생성하여 퍼포먼스를 향상합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>밉맵 생성</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;설정한 해상도로 텍스처를 리사이즈합니다. 해상도 값은 128, 256, 512, 1024, 2048, 4096, 8192로 설정 가능합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;원본 텍스처의 가로 세로 비율을 유지하며 설정한 해상도로 리사이즈합니다. 해상도 값은 128, 256, 512, 1024, 2048, 4096, 8192로 설정 가능합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;일부 게임은 압축된 UI 텍스처를 지원하지 않습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;텍스처 변환 작업 결과물의 포맷을 설정합니다. BC7은 스카이림SE와 폴아웃4의 경우에만 선택되어야 합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>출력 포맷</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;TGA 파일을 호환 가능한 DDS 포맷으로 변환합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>무손실 (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>애니메이션 처리</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;애니메이션 파일을 적합한 포맷으로 변환합니다. 애니메이션이 이미 호환 가능할 경우 변환하지 않습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>디버그 로그 기록</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>버그를 제보할 때 사용됩니다.</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>넥서스 페이지</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>디스코드 참여</translation>
    </message>
    <message>
        <source>About</source>
        <translation>정보</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Qt 에 대해..</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>튜토리얼 보기</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>로그 파일 열기</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>UI</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>UI용 텍스처 압축</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;해당 포맷을 사용하는 텍스처가 지원되는 포맷으로 변환됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>변환할 포맷</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>수정</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>애니메이션</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>로그</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>설정</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>어두운 테마</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>고급 설정</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>고급 설정은 사용자 지정 프로필을 사용하는 경우에만 변경할 수 있습니다.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>모드 묶음 작업 설정</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>여러 모드에 대해 일괄 작업을 진행합니다. 작업 시간이 증가할 수 있으며 특히 BSA 처리시 더 증가할 수 있습니다.</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>본 설정은 Mod Organizer의 mods 폴더에 사용할 수 있습니다.</translation>
    </message>
    <message>
        <source>
Made by G'k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
Made by G&apos;k
본 프로그램은 많은 이들에게 도움이 되고자 배포되었으나, 
어떠한 보증도 제공하지 않습니다. 
Mozilla Public License를 참조하십시오.</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>저장되지 않은 파일 저장하기</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>저장되지 않은 파일들이 있습니다. 파일을 저장할까요? &apos;모두 예(Yes to All)&apos; 를 눌러 자동 저장 기능을 활성화 할 수 있습니다.</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>새 프로필을 생성 합니다. &apos;CAO/profiles&apos; 경로에 새로운 폴더가 생성 됩니다.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>기본 프로필</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>어떤 프로필을 기본으로 사용하시겠습니까?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">에러</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation type="vanished">에러가 발생하여 처리가 중단되었습니다 :</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>완료</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>%1 %2에 어서오세요</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>이 메세지는 CAO를 처음 실행하면 나오는 메세지입니다. 
모든 옵션은 마우스를 대고 있으면 설명을 볼 수 있습니다. 
도움이 필요한 경우 개발자 디스코드에 참여해 주세요. 
어두운 테마는 상단의 옵션에서 적용할 수 있습니다.</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation>더미 ESP 생성.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;CAO는 일반적으로 사운드 파일을 포함한 비압축 BSA와 텍스처 BSA, 기타 파일을 묶은 스탠다드 BSA를 생성합니다. 본 옵션을 체크할 경우 모든 파일을 하나로 묶은 BSA를 생성합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation>BSA 갯수 최소화</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation>UI 저장</translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>대화창</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>검색</translation>
    </message>
</context>
</TS>