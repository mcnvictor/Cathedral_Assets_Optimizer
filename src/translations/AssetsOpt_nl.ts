<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Geeft aan welke bestanden aangepast zouden worden, zonder ze daadwerkelijk te verwerken</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Map openen</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>Uitvoeren</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>Eén mod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>Proefdraai</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Geavanceerde instellingen weergeven</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pakt BSAs die in de mod-map aanwezig zijn uit.&lt;span style=&quot; font-weight:600;&quot;&gt;Waarschuwing:&lt;/span&gt;Als je deze optie inschakelt, zal het proces aanzienlijk vertragen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>BSA uitpakken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Er wordt standaard een back-up gemaakt van bestaande BSAs. Wanneer deze optie is ingeschakeld worden back-ups uitgeschakeld.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>Back-ups verwijderen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creërt een nieuwe BSA van de bestaande losse bestanden.&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Waarschuwing:&lt;/span&gt;Als je deze optie inschakelt, zal het proces aanzienlijk vertragen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>BSA creëren</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Geavanceerd</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>Skyrim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>Maximumgrootte</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>Extensie</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>Achtervoegsel</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>Objecten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Probeert objecten te repareren die gegarandeerd het spel zullen crashen. Inclusief hoofddelen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>Vereiste optimalisatie</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>Gemiddelde optimalisatie</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optimaliseer alle objecten volledig. Alleen uitvoeren wanneer de standaardoptimalisatie niet alle nodige bestanden optimaliseerde. Kan kwaliteit verslechteren.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>Volledige optimalisatie</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>Altijd hoofddelen verwerken</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>Objecten heropslaan</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Stream</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>Texturen</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>Grootte aanpassen</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hoogte:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Breedte:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>Op verhouding</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>Op vastgestelde grootte</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>TGA-conversie inschakelen</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>Nieuw profiel</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nieuw</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Geeft aan welke bestanden aangepast zouden worden, zonder ze daadwerkelijk te verwerken. Negeert bestanden in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>Meerdere mods</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>BSAs verwerken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;De maximum ongecomprimeerde grootte van de bestanden in GB. Vergroten zal het aantal bestanden per BSA verhogen, maar je zou boven de limiet van BSA-groottes kunnen komen, wat het spel zal laten crashen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;De extensie van de BSA. Meestal zal dit &amp;quot;.bsa&amp;quot; voor Skyrim zijn en &amp;quot;.ba2&amp;quot; voor Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voorbeeld: &amp;quot; - Main.ba2&amp;quot; voor Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>Texturen-BSA creëren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voorbeeld: &amp;quot; - Textures.bsa&amp;quot; voor Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Expert</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSA-formaat</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>Texturen BSA-formaat</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>Objecten verwerken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Voer vereiste optimalisatie uit, en optimaliseert andere objecten lichtelijk. Dit zou visuele problemen op kunnen lossen, maar kan ook de kwaliteit verslechteren.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wanneer deze optie is uitgeschakeld, worden hoofddelen genegeerd. Aanbevolen om uit te schakelen wanneer meerdere mods worden verwerkt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Slaat objecten opnieuw op, zelfs als optimalisatie is uitgeschakeld. Voert dezelfde optimalisatie uit als het openen van een object in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>Texturen verwerken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converteert TSA-bestanden naar DDS, omdat SSE deze niet kan lezen. Probeert texturen te converteren en te repareren die het spel zouden crashen, omdat oudere formaten incompitabel zijn met SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Comprimeert ongecomprimeerde texturen naar het geconfigureerde formaat. Aanbevolen voor SSE en FO4, maar kan kwaliteit sterk verminderen voor andere games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>Texturen comprimeren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Genereert mipmaps for texturen, verbetert prestaties ten koste van hoger schijf- en VRAM-gebruik.</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>Mipmaps genereren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Past grootte van texturen aan, deelt de huidige breedte en hoogte door de gegeven waardes. Moet resulteren in afmetingen die in machten van 2 uit te drukken zijn, en groter dan 4x4.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Past grootte van textuurdimensies aan naar opgegeven groottes. Moet resulteren in afmetingen die in machten van 2 uit te drukken zijn, en groter dan 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sommige spellen ondersteunen geen gecomprimeerde interfacetexturen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uitvoerformaat voor ongecomprimeerde texturen, aangepaste texturen, en geconverteerde TGA-bestanden. BC7 alleen voor SSE of FO4 gebruiken.</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>Uitvoerformaat</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converteert TGAs naar een compitabel DDS-formaat als optie is ingeschakeld.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>Ongecomprimeerd (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>Animaties verwerken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converteert animaties naar het geschikte formaat. Als een animatie al compitabel is, worden er geen veranderingen toegepast.</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>Debuglog inschakelen</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>Gebruikt bij het rapporteren van bugs</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Documentatie</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discord</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Over Qt</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>Uitleg weergeven</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>Logbestand openen</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>Interfacetexturen comprimeren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Texturen die deze formaten gebruiken worden naar een ondersteund formaat geconverteerd.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>Ongewilde formaten</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bewerken</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>Animaties</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>Gereedschappen</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>Donker thema inschakelen</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>Geavanceerde instellingen</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>Geavanceerde instellingen kunnen alleen worden aangepast bij het gebruik van aangepaste profielen.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>Meerdere mods optie</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>Je hebt de optie voor meerdere mods ingeschakeld. Dit proces kan erg lang duren, vooral als je BSAs verwerkt.</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>Dit proces is alleen getest op de Mod Organizer mods map.</translation>
    </message>
    <message>
        <source>
Made by G'k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>Gemaakt door G&apos;k
Dit programma wordt gedeeld met de hoop dat het nuttig zal zijn maar GEEFT GEEN GARANTIE.
Zie de Mozilla Public License</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>Onopgeslagen aanpassingen opslaan</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>Je hebt onopgeslagen aanpassingen. Wil je ze opslaan? Je kunt op &apos;Ja voor alle&apos; klikken om altijd onopgeslagen aanpassingen op te slaan.</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>Je staat op het punt om een nieuw profiel aan te maken. Het zal een nieuwe map maken in &apos;CAO/profiles&apos;. Bekijk het alsjeblieft na het aanmaken, er zullen enkele bestanden in worden aangemaakt.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>Basisprofiel</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>Welk profiel wil je als basis gebruiken?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fout</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation type="vanished">Er is een fout opgetreden en het proces was geforceerd om te stoppen:</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>Welkom bij %1 %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>Het lijkt erop dat je CAO voor de eerste keer uitvoert. Alle opties hebben uitleg wanneer je je muis erop houdt. Als je hulp nodig hebt, kun je ons bereiken via Discord. Er is ook een donker thema beschikbaar.</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation>Dummy plug-ins creëren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Er zijn drie typen BSAs die CAO can creëren: oncomprimeerbare BSAs, BSAs voor texturen en standaard BSAs. De oncomprimeerbare bevat geluiden. Als deze optie is ingeschakeld, worden de drie typen BSAs samengevoegd indien mogelijk.</translation>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation>Zo min mogelijk BSAs creëren</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation>UI opslaan</translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
</context>
</TS>