<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>不实际处理文件，但会指明受到影响的文件</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>选择文件夹</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>运行</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>置入一个mod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>测试运行</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>显示高级设置</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>天际SE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;解包mod文件夹中的BSA文件。 &lt;span style=&quot; font-weight:600;&quot;&gt;注意&lt;/span&gt;！如果启用该选项，处理速度将会大大减慢。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>解包BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;正常情况下，处理完毕后会在原目录下生成一个.bak的备份文件。启用此选项将会删除这个备份。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>删除备份文件</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;在处理完松散文件形式的mod后，CAO将创建一个.bsa将其打包。&lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意&lt;/span&gt;！如果启用该选项，处理速度将会大大减慢。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>打包BSA</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>晨风</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>湮没</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>天际 / 辐射 3 / 辐射：新维加斯</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>辐射 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>最大容量</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>扩展</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>后缀名</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>网格</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>尝试修复会导致游戏崩溃的网格，包括头模。</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>必要优化</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>适中优化</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;极致优化所有网格。 仅在适中优化忽略必要文件的情况下适用。 可能会降低视觉质量。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>极致优化</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>处理头模</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>重新保存网格</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>湮没 (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>辐射 3 / 天际 / 天际 SE / 辐射 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>湮没 (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>辐射 3 / 天际 / 天际 SE / 辐射 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>流</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>湮没 / 辐射 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>天际 (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>天际 SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>辐射 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>纹理</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>调整大小</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>高度:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>宽度:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>按比例</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>按大小</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>启用TGA转换</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>配置方案</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>新建配置方案</translation>
    </message>
    <message>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;勾选后CAO将不会实际处理文件，而是在日志选项卡中列出所有被修改的文件，该选项忽略BSA中的文件。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>置入多个mod</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>BSA包处理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;设置BSA包中储存文件的最大值（以GB为单位）。 提高该数值会增加BSA包中所存储的文件大小，这样做可能会使BSA包大小达到硬限制，从而导致游戏崩溃。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;修改BSA包的扩展名。通常来说，应该是&amp;quot;.bsa&amp;quot;对应天际，&amp;quot;.ba2&amp;quot;对应辐射4。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;例如: &amp;quot; - Main.ba2&amp;quot;对应辐射 4。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>为textures文件夹单独创建BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;例如: &amp;quot; - Textures.bsa&amp;quot;对应天际SE。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>专业选项</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSA格式</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>Textures.bsa的格式</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>网格处理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在必要优化的基础上，并对其他网格进行细微优化。 这可能会修复一些视觉问题，但会降低其纹理质量。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;禁用此选项后，将忽略头模。 建议在处理多个mod时将其禁用。</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;无论是否优化，也将重新保存网格。并执行与在NifSkope中打开的网格相同程度的优化。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>纹理处理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将所有的TGA格式文件转换为DDS格式，因为天际SE无法读取TGA格式文件。 由于某些较旧的格式与天际SE不兼容，所以本选项会尝试转换并修复可能导致游戏崩溃的纹理。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将未压缩的纹理转制为支持的输出格式。 建议将其用于天际SE和辐射4，用于其他游戏可能会大大降低纹理质量。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>纹理处理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;生成纹理映射（mipmaps），通过提高磁盘和内存的使用来增强画面质量。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>生成纹理映射（mipmaps）</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;调整纹理的大小，将当前宽度和高度除以给定的值。必须以2的幂来表示纹理大小，不得低于 4x4 。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将纹理尺寸调整为给定尺寸。 必须以2的幂来表示纹理尺寸，不得低于4x4。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;某些游戏不支持压缩界面的纹理。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;未压缩纹理，已压缩纹理和TGA转换文件的输出格式。 提示：BC7格式仅适用于SSE或FO4。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>输出格式</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果选择该选项，则将TGA格式转换为兼容的DDS格式。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>未压缩格式： (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>动画处理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;将动画格式转换为适当的格式。 如果动画格式已经兼容，则不会进行任何转换。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>启用调试日志</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>用于处理bug</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>N网页面</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>讨论交流</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>关于Qt</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>使用警告信息</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>打开日志文件</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>界面</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>压缩界面纹理</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用这些格式的纹理将被转换为游戏支持的纹理格式。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>不需要的格式</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>修改</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>动画</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>日志</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>启用暗色主题</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>仅在使用自定义配置文件时才能修改高级设置。</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>多个mod选项</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>您选择了“置入多个mod”。 这可能需要很长时间，尤其是在需要处理BSA包时。 </translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>此过程仅在Mod Organizer mods文件夹中测试过。</translation>
    </message>
    <message>
        <source>
Made by G'k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
G&apos;k制作
此程序发布的初衷是希望能尽可能为你提供方便而不是做商业用途，具体请参照许可。（Jacky&amp;西瓜翻译：-）</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>保存更改</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>您尚未保存更改。是否保存？ 您也可以选择“全部同意”以始终保存更改</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>您将要创建一个新的配置文件。 CAO将在“/profiles”中创建一个新目录。 注意确认文件。</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>基础配置文件</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>您要使用哪个配置文件作为基础？</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation type="vanished">遇到异常，进程被迫停止：</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>欢迎使用 %1 %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>看来您是第一次运行CAO。 所有选项都有单独的说明。 如果您需要帮助，可以加入我们的讨论。 您可以选择使用暗色主题进行操作。</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation>创建虚拟插件</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;CAO能创建三种类型的BSA：未压缩BSA，纹理BSA和标准BSA。 未压缩BSA包含语音文件。如果激活此选项，这三种类型的BSA将尽可能合并到一起。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation>尽可能创建最少的BSA</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation>保存设置</translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
</TS>