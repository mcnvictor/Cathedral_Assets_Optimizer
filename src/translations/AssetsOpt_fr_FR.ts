<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr_FR" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Liste les fichiers qui seraient modifiés, sans les modifier réellement</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ouvrir le répertoire</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>Un seul mod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>Traitement à vide</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Montrer les paramètres avancés</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extrait tous les BSAs présents dans le dossier du mod. &lt;span style=&quot; font-weight:600;&quot;&gt;Attention&lt;/span&gt;.Si vous activez cette option, le processus sera considérablement ralenti.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>Extraire les BSAs</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Par défaut, un backup des BSAs existants est créé. Activer cette option désactivera ces backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>Suppression des backups</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Courier New&apos;;&quot;&gt;Crées un nouveau BSA après avoir traité les ressources précédemment extraites. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attention&lt;/span&gt;. Si vous activez cette option, le processus sera considérablement ralenti.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>Créer les BSAs</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>Skryim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>Taille maximale</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>Extension</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>Suffixe</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>Meshes</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essaye de réparer les meshes qui feront crasher le jeu. Les headparts sont inclus.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>Optimisation nécessaire</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>Optimisation intermédiaire</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optimise complètement toutes les meshes. Utilisez cette option seulement si les autres ont échoué. Pourrait diminuer la qualité visuelle.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>Optimisation complète</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>Toujours traiter les headparts</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>Re-sauvegarder les meshes</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>User</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Stream</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>Textures</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>Redimensionnage</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur:</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>Par ratio</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>Par taille fixe</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>Activer la conversion des TGAs</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>Nouveau profil</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Liste tous les fichiers qui seraient modifiés, sans les modifier réellement. Ignore les fichiers présents dans les BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>Plusieurs mods</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>Traiter les BSAs</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>La taille maximum non compressée des fichiers dans le BSA. L&apos;augmenter augmentera le nombre de fichiers stockés par BSA, mais vous pourriez atteindre la limite, causant des crashes en jeu.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>L&apos;extension du BSA. Généralement, ce sera &quot;.bsa&quot; pour Skyrim et &quot;.ba2&quot; pour Fallout 4.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Exemple : &quot; - Main.ba2&quot; pour Fallout 4.</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>Créer les BSAs de textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Exemple : &quot; - Textures.bsa&quot; pour Skyrim SE</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Expert</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>Format du BSA</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>Format du BSA de textures</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>Traiter les meshes</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Exécute l&apos;optimisation nécessaire, et optimise légèrement les autres meshes. Cela pourrait corriger quelques problèmes visuels, mais aussi diminuer la qualité.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Quand cette option est désactivée, les headparts sont ignorés. Il est recommandé de désactiver cette option quand plusieurs mods sont traités.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Re-sauvegarde les meshes même si l&apos;optimisation est désactivée. Exécutera la même optimization que si vous sauvegardiez un mesh dans NifSkope</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>Traiter les textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Convertit tous les fichiers TGA en DDS, car SSE ne peut les lire. Essaye de convertir et de corriger les textures qui feraient crasher le jeu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Compresse les textures non compressées vers le format de sortie sélectionné. Cela est recommandé pour SSE et FO4, but pourrait hautement dégrader la qualité pour les autres jeux.</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>Compresser les textures</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Génère des mipmaps pour les textures, améliorant les performances mais augmentant l&apos;utilisation du disque et de la vram.</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>Générer les mipmaps</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Redimensionne les textures, divisant les hauteur et largeur actuelles par les valeurs données. Doit résulter dans des dimensions exprimées en puissances de deux, supérieures à 4x4.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Redimensionne les textures vers la taille donnée. Doit résulter dans des dimensions exprimées en puissances de deux, supérieures à 4x4.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Certains jeux ne supportent pas la compression des textures de l&apos;interface</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Format de sortie pour les textures non compressées, les textures modifiées, et les fichiers TGA convertis. BC7 devrait seulement être utilisé pour SSE et FO4.</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>Format de sortie</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Convertit les TGA vers un format DDS compatible.</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>Uncompressed (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>Traiter les animations</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Convertit les animations vers un format approprié. Si l&apos;animation est déjà compatible, aucun changement ne sera fait.</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>Activer le log de debug</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>Utilisé pour signaler des bugs</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Documentatio</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discor</translation>
    </message>
    <message>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>A propos de Qt</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>Montrer les tutoriels</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>Ouvrir le fichier journal</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>Compression des textures d&apos;interface</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Les textures utilisant ces formats seront converties vers un format supporté.</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>Format non souhaités</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>Animations</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Journal</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>Activer le thème sombre</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>Paramètres avancés</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>Les paramètres avancés peuvent seulement être modifiés lors de l&apos;utilisation de profils personnalisés.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>Option &quot;Plusieurs mods&quot;</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>Vous avez sélectionne l&apos;option &quot;plusieurs mods&quot;. Ce processus pourrait prendre un très long moment, particulièrement si vous traitez les BSAs.</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>Ce processus a seulement été testé sur le dossier mods de Mod Organizer.</translation>
    </message>
    <message>
        <source>
Made by G'k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>Créé par G&apos;k
Ce programme est distribué dans le but qu&apos;il soit utile, mais SANS AUCUNE GARANTIE. Voir la Mozilla Public License.</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>Sauvegarder les changements</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>Vous avez des changements non sauvegardés. Voulez-vous les sauvegarder ? Vous pouvez aussi appuyer sur &quot;Oui pour tous&quot; pour qu&apos;ils soient toujours sauvegardés.</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>Vous vous apprêtez à créer un nouveau profil. Ceci créera un nouveau dossier dans CAO/profiles. Veuillez le vérifier après sa création, il contiendra plusieurs fichiers.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>Profile de base</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>Sur quel profile voulez-vous vous baser ?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation type="vanished">Une exception a été rencontrée et le programme a été contraint de s&apos;arrêter:</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>Bienvenue dans %1 %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>Il semblerait que vous lanciez CAO pour la première fois. Toutes les options ont des infobulles expliquant leur utilité. Si vous avez besoin d&apos;aide, vous pouvez nous rejoindre sur Discord. Un thème sombre est aussi disponible.</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation>Créer des dummy plugins</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;CAO peut créer trois types de BSAs : incompressible, texture et standard. Le BSA incompressible contient les sons. Si cette option est activée, les trois types de BSAs seront fusionnés si possible.</translation>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation>Créer le moins de BSAs possible</translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation>Sauvegarder les paramètres</translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
</context>
</TS>