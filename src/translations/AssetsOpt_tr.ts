<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <source>Cathedral Assets Optimizer</source>
        <translation>Cathedral Assets Optimizer</translation>
    </message>
    <message>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Dosyaları işlemeden, hangilerinin etkileneceğini gösterir</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Dizini Aç</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>Başlat</translation>
    </message>
    <message>
        <source>One mod</source>
        <translation>Tek Mod</translation>
    </message>
    <message>
        <source>Dry run</source>
        <translation>Prova Modu</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Gelişmiş ayarları göster</translation>
    </message>
    <message>
        <source>SSE</source>
        <translation>SSE</translation>
    </message>
    <message>
        <source>BSA</source>
        <translation>BSA</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder. &lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mod klasöründe bulunan tüm BSA arşivlerini çıkartır. &lt;span style=&quot; font-weight:600;&quot;&gt;Uyarı&lt;/span&gt;: Bu seçeneği işaretlerseniz, işleme süreci kaydadeğer ölçüde yavaşlar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Extract BSA</source>
        <translation>BSA Arşivlerini Çıkart</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, a backup of existing bsa is created. Enabling this option will disable those backups.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Varsayılan olarak, varolan BSA arşivlerinin yedeği alınır. Bu seçeneği işaretlemek, yedek alma işlemini devre dışı bırakır.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Delete backups </source>
        <translation>Yedekleri sil</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Creates a new BSA, packing the existing loose files. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;. If you enable this option, the process will be considerably slowed down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span&gt;Varolan açıktaki dosyaları kullanarak bir BSA arşivi oluşturur. &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Uyarı&lt;/span&gt;: Bu seçeneği işaretlerseniz, işleme süreci kaydadeğer ölçüde yavaşlar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create BSA</source>
        <translation>BSA Arşivi Oluştur</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <source>Morrowind</source>
        <translation>Morrowind</translation>
    </message>
    <message>
        <source>Oblivion</source>
        <translation>Oblivion</translation>
    </message>
    <message>
        <source>Skyrim / Fallout 3 / Fallout New Vegas</source>
        <translation>Skyrim / Fallout 3 / Fallout New Vegas</translation>
    </message>
    <message>
        <source>Fallout 4</source>
        <translation>Fallout 4</translation>
    </message>
    <message>
        <source>Maximum size</source>
        <translation>Toplam Boyut</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>Uzantı</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation>Ek</translation>
    </message>
    <message>
        <source>Meshes</source>
        <translation>Meshler</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to repair meshes which are guaranteed to crash the game. Headparts are included.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Oyunun kesin olarak çökmesine sebep olacak meshleri onarmaya çalışır. Headpart dosyaları (saç, sakal vb.) buna dahildir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Necessary optimization</source>
        <translation>Gerekli Optimizasyon</translation>
    </message>
    <message>
        <source>Medium optimization</source>
        <translation>Orta Optimizasyon</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fully optimize all meshes. Only apply if standard mesh optimization ignored necessary files. May lower visual quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tüm meshleri optimize eder. Yalnızca, normal mesh optimizasyonu gerekli dosyaları işlemediyse kullanın. Kaliteyi düşürebilir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Full optimization</source>
        <translation>Tam Optimizasyon</translation>
    </message>
    <message>
        <source>Always process headparts</source>
        <translation>Headpart dosyalarını (saç, sakal vb.) her zaman işle</translation>
    </message>
    <message>
        <source>Resave meshes</source>
        <translation>Meshleri tekrar kaydet</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <source>Oblivion (V20_0_0_5)</source>
        <translation>Oblivion (V20_0_0_5)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (V20_2_0_7)</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Kullanıcı</translation>
    </message>
    <message>
        <source>Oblivion (11)</source>
        <translation>Oblivion (11)</translation>
    </message>
    <message>
        <source>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</source>
        <translation>Fallout 3 / Skyrim / Skyrim SE / Fallout 4 (12)</translation>
    </message>
    <message>
        <source>Stream</source>
        <translation>Sınıflandırma</translation>
    </message>
    <message>
        <source>Oblivion / Fallout 3 (82)</source>
        <translation>Oblivion / Fallout 3 (82)</translation>
    </message>
    <message>
        <source>Skyrim (83)</source>
        <translation>Skyrim (83)</translation>
    </message>
    <message>
        <source>Skyrim SE (100)</source>
        <translation>Skyrim SE (100)</translation>
    </message>
    <message>
        <source>Fallout 4 (130)</source>
        <translation>Fallout 4 (130)</translation>
    </message>
    <message>
        <source>Textures</source>
        <translation>Dokular</translation>
    </message>
    <message>
        <source>Resizing</source>
        <translation>Yeniden Boyutlandırma</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Yükseklik</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Genişlik</translation>
    </message>
    <message>
        <source>By ratio</source>
        <translation>Orana Göre</translation>
    </message>
    <message>
        <source>By fixed size</source>
        <translation>Belirlenen Boyuta Göre</translation>
    </message>
    <message>
        <source>TGA</source>
        <translation>TGA</translation>
    </message>
    <message>
        <source>Enable TGA conversion</source>
        <translation>TGA dönüştürülmesini etkinleştir</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>New profile</source>
        <translation>Yeni Profil</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Yeni</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will list all files that would be modified, without actually processing them. Will ignore files in BSAs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dosyaları işlemeden, hangilerinin etkileneceğini listeler. BSA arşivlerindeki dosyaları yoksayar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Several mods </source>
        <translation>Birden fazla Mod</translation>
    </message>
    <message>
        <source>Process BSAs</source>
        <translation>BSA arşivlerini işle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum uncompressed size of the files in GB. Increasing it will increase the number of files stored per BSA, but you might reach the hard limit of BSA size, causing the game to crash.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dosyaların, sıkıştırılmamış hallerindeki toplam boyutları (GB). Bu sayıyı yükseltmek, her BSA arşivinde bulunan dosyaların sayısını artırarak oluşturulacak BSA arşivlerinin sayısını azaltır fakat oyuna kodlanmış maksimum BSA arşivi boyutu sınırını aşarsanız oyununuzun çökmesine sebep olabilirsiniz.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The extension of the bsa. Usually, this will be &amp;quot;.bsa&amp;quot; for Skyrim, &amp;quot;.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BSA arşivinin uzantısı. Genellikle, Skyrim için &amp;quot;.bsa&amp;quot; Fallout 4 içinse, &amp;quot;.ba2&amp;quot; şeklindedir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Main.ba2&amp;quot; for Fallout 4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Örnek :Fallout 4 için, &amp;quot; - Main.ba2&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Create textures BSA</source>
        <translation>Dokular için BSA Arşivi Oluştur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Example : &amp;quot; - Textures.bsa&amp;quot; for Skyrim SE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Örnek : Skyrim SE için &amp;quot; - Textures.bsa&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Uzman</translation>
    </message>
    <message>
        <source>BSA Format</source>
        <translation>BSA Formatı</translation>
    </message>
    <message>
        <source>Textures BSA Format</source>
        <translation>Doku dosyalarının BSA Formatı</translation>
    </message>
    <message>
        <source>Process meshes</source>
        <translation>Meshleri işle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform necessary optimization, and also lightly optimizes other meshes. This may fix some visual issues, but may also lower quality. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gerekli optimizasyonları gerçekleştirir ve diğer meshleri hafifçe optimize eder. Bu seçenek, görsel sorunları düzeltebilir fakat kaliteyi düşürebilir. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is disabled, headparts are ignored. It is recommended to keep it disabled when processing several mods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bu seçenek seçili değilken headpart dosyaları (saç, sakal vb.) yoksayılır. Birden fazla mod işlerken bu seçeneği seçmemeniz önerilir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will resave meshes even if optimization is disabled. Will perform the same optimization as opening a mesh in NifSkope.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optimizasyon devre dışı bırakıldıysa bile meshleri tekrar kaydeder. Bir mesh dosyasını NifSkope&apos;da açmakla aynı işlemi gerçekleştirir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Process textures</source>
        <translation>Dokuları işle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts any TGA files into DDS, as SSE cannot read these. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;SSE, .TGA uzantılı dosyaları okuyamadığı için bu türdeki tüm dosyaları .DDS formatına dönüştürür. Bazı eski formatlar SSE ile uyumlu olmadığından oyunun çökmesine sebep olabilecek tüm doku dosyalarını dönüştürmeye ve onarmaya çalışır.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Compresses uncompressed textures to the configured output format. It is recommended for SSE and FO4, but may highly decrease quality for other games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sıkıştırılmamış dosyaları, yapılandırılan çıktı formatında sıkıştırır. SSE ve FO4 için önerilir fakat diğer oyunlarda kaliteyi gözle görülür ölçüde düşürebilir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Compress textures</source>
        <translation>Dokuları sıkıştır</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generates mipmaps for textures, improving the performance at the cost of higher disk and vram usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Doku dosyaları için mipmap oluşturarak daha yüksek disk ve vram kullanımı karşılığında performansı iyileştirir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate mipmaps</source>
        <translation>Mipmap oluştur</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes textures, dividing the current width and height by the values given. Must result in texture dimensions expressed in powers of two, no less than 4x4. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Yazılı olan değerlerde yüksekliği ve genişliği bölerek dokuları yeniden boyutlandırır. Boyutlandırılan dosya, yeniden boyutlandırıldığında, 4x4&apos;den küçük olmamalı ve ikinin katı bir boyuta sahip olmalıdır. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resizes texture dimensions to the given sizes. Must result in texture dimensions expressed in powers of two, no less than 4x4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dosyayı yazılı değerleri kullanarak yeniden boyutlandırır. Boyutlandırılan dosya, yeniden boyutlandırıldığında, 4x4&apos;den küçük olmamalı ve ikinin katı bir boyuta sahip olmalıdır..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some games do not support compressed interface textures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bazı oyunlar sıkıştırılmış arayüz dokularını desteklemez.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output format for uncompressed textures, modified textures, and converted TGA files. BC7 should only be used for SSE or FO4.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sıkıştırılmamış, düzenlenmiş doku dosyalarının ve donüştürülmüş TGA dosyalarının çıktı formatı. SSE ve FO4 için BC7 kullanılmalıdır.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Output format</source>
        <translation>Çıktı Formatı</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Will convert TGAs to a compatible DDS format if option is selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;İşaretlenirse, TGA dosyalarını uyumlu bir DDS formatına dönüştürür.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>BC7 (BC7_UNORM)</source>
        <translation>BC7 (BC7_UNORM)</translation>
    </message>
    <message>
        <source>BC3 (BC3_UNORM)</source>
        <translation>BC3 (BC3_UNORM)</translation>
    </message>
    <message>
        <source>BC1 (BC1_UNORM)</source>
        <translation>BC1 (BC1_UNORM)</translation>
    </message>
    <message>
        <source>Uncompressed (R8G8B8A8_UNORM)</source>
        <translation>Sıkıştırılmamış (R8G8B8A8_UNORM)</translation>
    </message>
    <message>
        <source>Process animations</source>
        <translation>Animasyonları işle</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to the appropriate format. If an animation is already compatible, no change will be made.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Animasyonları uygun bir formata dönüştürür. Bir animasyon dosyası zaten uyumlu bir formata sahipse hiçbir değişiklik yapılmaz.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <source>Enable debug log</source>
        <translation>Hata ayıklama günlüğünü etkinleştir</translation>
    </message>
    <message>
        <source>Used when reporting bugs</source>
        <translation>Hata bildirirken kullanılır</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation>Belgelendirme</translation>
    </message>
    <message>
        <source>Discord</source>
        <translation>Discord</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Qt Hakkında</translation>
    </message>
    <message>
        <source>Show tutorials</source>
        <translation>Eğiticileri Göster</translation>
    </message>
    <message>
        <source>Open log file</source>
        <translation>Günlük dosyasını aç</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation>Arayüz</translation>
    </message>
    <message>
        <source>Compress interface textures</source>
        <translation>Arayüz dokularını sıkıştır</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Textures using these formats will be converted to a supported format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Bu formattaki tüm dokular desteklenen bir formata dönüştürülür.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unwanted formats</source>
        <translation>İstenmeyen Formatlar</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <source>Animations</source>
        <translation>Animasyonlar</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Günlük</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>Araçlar</translation>
    </message>
    <message>
        <source>Enable dark theme</source>
        <translation>Karanlık Modu Etkinleştir</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation>Gelişmiş Ayarlar</translation>
    </message>
    <message>
        <source>Advanced settings can only be modified when using custom profiles.</source>
        <translation>Gelişmiş ayarlar sadece özel profil kullanırken değiştirilebilir.</translation>
    </message>
    <message>
        <source>Several mods option</source>
        <translation>Birden fazla Mod seçeneği</translation>
    </message>
    <message>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. </source>
        <translation>Birden fazla mod seçeneğini işaretlediniz. Özellike BSA arşivi işliyorsanız, bu işlem uzun sürebilir.</translation>
    </message>
    <message>
        <source>This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>Bu işlem sadece Mod Organizer&apos;ın modlar klasöründe test edilmiştir.</translation>
    </message>
    <message>
        <source>
Made by G'k
This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTLY. See the Mozilla Public License</source>
        <translation>
Bu program, G&apos;k Tarafından yazılmıştır.
Bu programın dağıtımı, yararlı olabilmesi adına yapılmıştır fakat BİR TEMİNAT SAĞLANMAMAKTADIR. Mozilla Toplum Lisansına bakınız.</translation>
    </message>
    <message>
        <source>Save unsaved changes</source>
        <translation>Kaydedilmemiş değişiklikleri kaydet</translation>
    </message>
    <message>
        <source>You have unsaved changes. Do you want to save them? You can also press &apos;Yes to all&apos; to always save unsaved changes</source>
        <translation>Kaydedilmemiş değişiklikleriniz mevcut. Bu değişiklikleri kaydetmek ister misiniz? Ayrıca, her zaman kaydedilmemiş değişiklikleri kaydetmek için &apos;Hepsine evet&apos;e tıklayabilirsiniz.</translation>
    </message>
    <message>
        <source>You are about to create a new profile. It will create a new directory in &apos;CAO/profiles&apos;. Please check it out after creation, some files will be created inside it.</source>
        <translation>Yeni bir profil oluşturmak üzeresiniz. Bu işlem, &apos;CAO/profiles&apos; dizini içinde yeni bir klasör oluşturur. Lütfen, profili oluşturduktan sonra bu klasörü kontrol edin içinde bazı dosyalar oluşturulacak.</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>İsim:</translation>
    </message>
    <message>
        <source>Base profile</source>
        <translation>Taban Alınacak Profil</translation>
    </message>
    <message>
        <source>Which profile do you want to use as a base?</source>
        <translation>Hangi profili taban almak istiyorsunuz?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Hata</translation>
    </message>
    <message>
        <source>An exception has been encountered and the process was forced to stop: </source>
        <translation type="vanished">Bir istisna meydana geldi ve işlem zorla durduruldu:</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <source>Welcome to %1 %2</source>
        <translation>%1&apos;a Hoş geldiniz. Sürüm: %2</translation>
    </message>
    <message>
        <source>It appears you are running CAO for the first time. All options have tooltips explaining what they do. If you need help, you can also join us on Discord. A dark theme is also available.</source>
        <translation>CAO&apos;yu ilk kez çalıştırıyorsunuz. Her seçeneğin ne yaptığını açıklayan kendi bilgiçubuğu mevcuttur. Yardıa ihtiyacınız olursa Discord sunucumuza katılabilirsiniz. Ayrıca, Programda Karanlık Mod seçeneği mevcuttur.</translation>
    </message>
    <message>
        <source>Create dummy plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;There are three types of BSAs CAO can create : uncompressible, textures and standard. The uncompressible one contains sounds. If this option is checked, these three types of BSAs will be merged if possible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Create the least BSAs possible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save UI</source>
        <translation>Kullanıcı Arayüzünü Kaydet</translation>
    </message>
</context>
<context>
    <name>TexturesFormatSelectDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Pencere</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
</context>
</TS>